#include "bitset.h"

#include <cstring>
#include <cassert>
#include <cstdio>

// just popcount since c++20
size_t countBits(uint32_t x)
{
    return __builtin_popcount(x);
}

inline size_t countBits(uint64_t x)
{
    return __builtin_popcountll(x);
}

Bitset::Bitset(bool set)
{
    if (set)
    {
        setAll();
    }
    else
    {
        resetAll();
    }
}

Bitset::Bitset(const Bitset& other)
{
    *this = other;
}

Bitset::~Bitset() = default;

void Bitset::set(size_t n)
{
    mValues[n >> UNIT_LOG_BITS_COUNT] |= UNIT << (n & (UNIT_BITS_COUNT - 1));
}

void Bitset::reset(size_t n)
{
    mValues[n >> UNIT_LOG_BITS_COUNT] &= ~(UNIT << (n & (UNIT_BITS_COUNT - 1)));
}

void Bitset::setAll()
{
    memset(mValues, 255, sizeof(Storage));
}

void Bitset::resetAll()
{
    memset(mValues, 0, sizeof(Storage));
}

bool Bitset::test(size_t n)
{
    return mValues[n >> UNIT_LOG_BITS_COUNT] & (UNIT << (n & (UNIT_BITS_COUNT - 1)));
}

void Bitset::flip(size_t n)
{
    mValues[n >> UNIT_LOG_BITS_COUNT] ^= ~(UNIT << (n & (UNIT_BITS_COUNT - 1)));
}

Bitset& Bitset::operator|=(const Bitset& other)
{
    for (size_t i = 0; i < UNITS_COUNT; ++i)
    {
        mValues[i] |= other.mValues[i];
    }

    return *this;
}

Bitset& Bitset::operator&=(const Bitset& other)
{
    for (size_t i = 0; i < UNITS_COUNT; ++i)
    {
        mValues[i] &= other.mValues[i];
    }

    return *this;
}

Bitset& Bitset::operator=(const Bitset& other)
{
    memcpy(mValues, other.mValues, sizeof(Storage));

    return *this;
}

Bitset Bitset::operator|(const Bitset& other) const
{
    Bitset result{*this};
    result |= other;
    return result;
}

Bitset Bitset::operator&(const Bitset& other) const
{
    Bitset result{*this};
    result &= other;
    return result;
}

Bitset Bitset::operator~() const
{
    Bitset result{*this};

    for (size_t i = 0; i < UNITS_COUNT; ++i)
    {
        result.mValues[i] = ~mValues[i];
    }

    return result;
}

size_t Bitset::count() const
{
    size_t result = 0;

    for (size_t i = 0; i < UNITS_COUNT; ++i)
    {
        result += countBits(mValues[i]);
    }

    return result;
}

bool Bitset::all() const
{
    for (size_t i = 0; i < UNITS_COUNT; ++i)
    {
        if (mValues[i] != UNIT_MAX_VALUE)
        {
            return false;
        }
    }

    return true;
}

bool Bitset::any() const
{
    for (size_t i = 0; i < UNITS_COUNT; ++i)
    {
        if (mValues[i])
        {
            return true;
        }
    }

    return false;
}

bool Bitset::none() const
{
    return not any();
}

bool Bitset::operator==(const Bitset& other) const
{
    for (size_t i = 0; i < UNITS_COUNT; ++i)
    {
        if (mValues[i] != other.mValues[i])
        {
            return false;
        }
    }

    return true;
}

bool Bitset::operator!=(const Bitset& other) const
{
    return not (*this == other);
}

Bitset::ByIndexGetter Bitset::operator[](size_t n)
{
    return ByIndexGetter(*this, n);
}

Bitset::ByIndexGetter::ByIndexGetter(Bitset& bitset, size_t n)
    : mOrigin{bitset}
    , mN{n}
{
}

Bitset::ByIndexGetter& Bitset::ByIndexGetter::operator=(bool value)
{
    if (value)
    {
        mOrigin.set(mN);
    }
    else
    {
        mOrigin.reset(mN);
    }

    return *this;
}

Bitset::ByIndexGetter::operator bool() const
{
    return mOrigin.test(mN);
}
