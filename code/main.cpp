#include "bitset.h"

#include <cassert>
#include <cstdio>

int main()
{
    Bitset bitset{};
    assert(bitset.count() == 0);

    for (int i = 0; i < Bitset::SIZE / 3; ++i)
    {
        bitset.set(i * 3);
    }

    assert(bitset.count() == Bitset::SIZE / 3);

//    auto x = bitset[5];

    assert(bitset.any());
    assert(not bitset.none());
    assert(not bitset.all());

    bitset[5] = true;

    assert(bitset[6]);

    bitset[32] = true;
    assert(bitset[32]);
    assert(not bitset[31]);

    bitset.setAll();
    assert(bitset.any());
    assert(not bitset.none());
    assert(bitset.all());

    bitset.resetAll();
    assert(not bitset.any());
    assert(bitset.none());
    assert(not bitset.all());

    bitset = ~bitset;
    assert(bitset == Bitset{true});

    return 0;
}
