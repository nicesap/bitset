#pragma once

#include <cstddef>
#include <cstdint>

// Good enough.
constexpr size_t divideRoundUp(size_t a, size_t b)
{
    return (a + b - 1) / b;
}

constexpr size_t log2(size_t x)
{
    for (size_t i = 0; ; ++i)
    {
        if ((size_t{1} << i) == x)
        {
            return i;
        }
    }
}

class Bitset final
{
private:
    class ByIndexGetter final
    {
    public:
        explicit ByIndexGetter(Bitset& origin, size_t n);

        ByIndexGetter() = delete;
        ByIndexGetter(const ByIndexGetter&) = delete;

        ByIndexGetter& operator=(bool value);
        operator bool() const;

    private:
        Bitset& mOrigin;
        size_t mN;
    };

public:
    static constexpr size_t SIZE = 10000000;

    explicit Bitset(bool set = false);

    Bitset(const Bitset& other);
    ~Bitset();

    void set(size_t n);
    void reset(size_t n);

    void setAll();
    void resetAll();

    bool test(size_t n);
    void flip(size_t n);

    Bitset& operator|=(const Bitset& other);
    Bitset& operator&=(const Bitset& other);
    Bitset& operator=(const Bitset& other);
    Bitset operator|(const Bitset& other) const;
    Bitset operator&(const Bitset& other) const;
    Bitset operator~() const;

    size_t count() const;

    bool all() const;
    bool any() const;
    bool none() const;
    bool operator==(const Bitset& other) const;
    bool operator!=(const Bitset& other) const;

    Bitset operator<<(size_t shift) const;
    Bitset operator>>(size_t shift) const;
    Bitset& operator<<=(size_t shift);
    Bitset& operator>>=(size_t shift);

    ByIndexGetter operator[](size_t n);
// second version is prefered as less error-prone (makes auto-mistake impossible)
//    ByIndexGetter&& operator[](size_t n) const;

private:
    static_assert(SIZE > 0);
    using Unit = uint32_t;
    static constexpr Unit UNIT = 1;
    static constexpr size_t UNIT_BITS_COUNT = sizeof(Unit) * 8;
    static constexpr size_t UNIT_LOG_BITS_COUNT = log2(UNIT_BITS_COUNT);
    static constexpr size_t UNITS_COUNT = divideRoundUp(SIZE, UNIT_BITS_COUNT);
    static constexpr Unit UNIT_MAX_VALUE = -1;
    static_assert(UNIT_MAX_VALUE + 1 == 0);

    using Storage = Unit[UNITS_COUNT];

    Storage mValues;
};
