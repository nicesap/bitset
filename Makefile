target/bitset: code/bitset.h code/bitset.cpp code/main.cpp | target
	g++ -fsanitize=address --std=c++17 code/bitset.cpp code/main.cpp -o target/bitset

target:
	mkdir target

clean:
	rm -rf target
